package password;

/**
 * 
 * @author Samandeep Singh 991500155
 *
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	private static int MIN_DIGIT_COUNT = 2;
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	public static boolean isValidLength(String password) {
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int digitCount = 0;
		for(char c : password.toCharArray()) {
			if(Character.isDigit(c)) digitCount++;
		}
		return (digitCount >= MIN_DIGIT_COUNT);
	}

}
